package main

import (
	"encoding/json"
	"github.com/aerospike/aerospike-client-go"
	"io/ioutil"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

type Host struct {
	Name string `json:"name"`
	Port int    `json:"port"`
}

type Settings struct {
	Hosts              []Host `json:"hosts"`
	UseConnectionCount int    `json:"use_connection_count"`
	Namespace          string `json:"namespace"`
	SetName            string `json:"set_name"`
	KeyPrefix          string `json:"key_prefix"`
	Expired            int    `json:"expired"`
	MinValueSize       int    `json:"min_value_size"`
	MaxValueSize       int    `json:"max_value_size"`
	Times              int    `json:"times"`
	ParallelCount      int    `json:"parallel_count"`
	Delay              int    `json:"delay"`
}

func main() {
	var args = os.Args[1:]

	if len(args) < 2 {
		log.Fatal(`expected "command name" and "settings.json" as "go run main.go generate|fill some-settings.json"`)
	}

	var command, settingsPath = args[0], args[1]

	var path, pathErr = filepath.Abs(settingsPath)
	if pathErr != nil {
		log.Fatalf(`expected "command name" and "settings.json"`)
	}

	switch command {
	case "generate":
		generate(path)
	case "fill":
		fill(fetchSettingsByPath(path))
	}
}

func generate(path string) {
	var _, existsErr = os.Stat(path)

	if os.IsNotExist(existsErr) {
	} else {
		log.Fatalf(`file "%s" already exists`, path)
	}

	var settings = Settings{
		Hosts: []Host{
			{
				Name: "localhost",
				Port: 3100,
			},
			{
				Name: "localhost",
				Port: 3100,
			},
		},
		UseConnectionCount: 100,
		Namespace:          "test",
		SetName:            "test",
		KeyPrefix:          "x",
		Expired:            86400,
		MinValueSize:       1024,
		MaxValueSize:       8096,
		Times:              1000,
		ParallelCount:      16,
		Delay:              0,
	}

	var content, marshalErr = json.MarshalIndent(&settings, "", "\t")
	if marshalErr != nil {
		log.Fatalf(`cannot marshal settings to store into "%s"`, path)
	}

	var writeErr = ioutil.WriteFile(path, content, 0666)
	if writeErr != nil {
		log.Fatalf(`cannot write into "%s"`, path)
	}

	log.Printf(`success genrated "%s" with default settings`, path)
}

func fill(settings Settings) {
	var (
		namespace          = settings.Namespace
		setName            = settings.SetName
		parallelCount      = settings.ParallelCount
		times              = settings.Times
		expired            = settings.Expired
		keyPrefix          = settings.KeyPrefix
		minValueSize       = settings.MinValueSize
		maxValueSize       = settings.MaxValueSize
		useConnectionCount = settings.UseConnectionCount
		delay              = time.Duration(settings.Delay)
	)

	var clients = make([]*aerospike.Client, useConnectionCount)
	{
		var startTime = time.Now()
		for i := 0; i < useConnectionCount; i++ {
			var client, err = client(settings.Hosts)
			if err != nil {
				log.Printf("connection %d error %+v", i, err)

				return
			}
			defer client.Close()

			clients[i] = client
		}
		log.Printf("success create %d connections by %s \n", useConnectionCount, time.Since(startTime))
	}

	{
		var startTime = time.Now()
		var policy = aerospike.NewWritePolicy(0, uint32(expired))
		policy.TotalTimeout = time.Second

		var workerCount = 1
		if parallelCount > workerCount {
			workerCount = parallelCount
		}

		var (
			successComplete uint64 = 0
			totalStoreSize  uint64 = 0
		)

		var worker = func(wg *sync.WaitGroup, jobQueue <-chan int) {
			defer wg.Done()

			var value = make([]byte, maxValueSize)

			for i := range jobQueue {
				var key, _ = aerospike.NewKey(namespace, setName, aerospike.NewStringValue(keyPrefix+strconv.Itoa(math.MaxInt32+i)))

				var currentChange = i % maxValueSize
				var currentSize = maxValueSize - i%minValueSize

				value[currentChange] = byte(i)
				var bytesValue = aerospike.NewBytesValue(value[:currentSize])

				var client = clients[i%useConnectionCount]
				var putErr = client.PutBins(policy, key, aerospike.NewBin("value", bytesValue))
				if putErr != nil {
					log.Printf("put error %+v", putErr)

					break
				}

				atomic.AddUint64(&totalStoreSize, uint64(currentChange))
				atomic.AddUint64(&successComplete, 1)

				time.Sleep(delay)
			}
		}

		var jobQueue = make(chan int, workerCount)

		var wg = new(sync.WaitGroup)
		for i := 0; i < workerCount; i++ {
			wg.Add(1)

			go worker(wg, jobQueue)
		}

		for i := 0; i < times; i++ {
			jobQueue <- i
		}
		close(jobQueue)

		wg.Wait()

		log.Printf("success complete %d from %d and store %d bytes by %s \n", successComplete, times, totalStoreSize, time.Since(startTime))
	}
}

func client(source []Host) (*aerospike.Client, error) {
	policy := aerospike.NewClientPolicy()

	policy.Timeout = 3 * time.Second
	policy.ConnectionQueueSize = 512

	var credentials = make([]*aerospike.Host, 0, len(source))
	for _, host := range source {
		credentials = append(credentials, aerospike.NewHost(host.Name, host.Port))
	}

	return aerospike.NewClientWithPolicyAndHost(policy, credentials...)
}

func fetchSettingsByPath(path string) Settings {
	var content, readErr = ioutil.ReadFile(path)
	if readErr != nil {
		log.Fatalf(`cannot read file "%s" by error "%+v"`, path, readErr)
	}

	var result = Settings{}
	var unmarshalErr = json.Unmarshal(content, &result)
	if unmarshalErr != nil {
		log.Fatalf(`cannot unmarshal file "%s" by error "%+v"`, path, unmarshalErr)
	}

	return result
}
